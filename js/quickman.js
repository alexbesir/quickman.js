
// Manual data holder
var quickman_data = [];

$(document).ready(function(){

	// Make sure the trim() function exists
	if(!String.prototype.trim) {  
	  String.prototype.trim = function () {  
		return this.replace(/^\s+|\s+$/g,'');  
	  };  
	} 

	// Get all text between <xmp> tags
	var qm = $("xmp").html();

	// Prepare internal variables
	var titleSet = false;
	var section = 0;
	var subsection = 0;
	
	// Read qm line by line and create manual
	qm = qm.split("\n");
	var i = 0;
	for(var iter = 0; iter < qm.length; iter++) {
	
		// Trim
		var line = qm[iter].trim();
	
		// If first character is #, create new section
		if(line.substring(0,2) === "# ") {
		
			section++;
			subsection = 0;
			quickman_data[i] = ["s",section+" &nbsp;",line.substring(2)];
			
		}
		
		// If first two caracters are ##, create new sub-section
		else if(line.substring(0,3) == "## ") {
		
			subsection++;
			quickman_data[i] = ["ss",section+"."+subsection+" &nbsp;",line.substring(3)];
		
		}
		
		// If first three caracters are ###, create new sub-sub-section
		else if(line.substring(0,4) === "### ") {
		
			quickman_data[i] = ["sss","",line.substring(4)];
		
		}
		
		// If first character is >, create image
		else if(line.substring(0,2) === "> ") {
		
			quickman_data[i] = ["i","",line.substring(2)];
		
		}
		
		// If first character is -, create list item
		else if(line.substring(0,2) === "- ") {
		
			quickman_data[i] = ["l","",line.substring(2)];
		
		}
		
		// If empty string, skip
		else if(line.length == 0) { continue; }
		
		// Otherwise create paragraph
		else {
		
			// If title not set yet, this is title
			if(!titleSet) {
				quickman_data[i] = ["t","",line];
				titleSet = true;
			}
			else quickman_data[i] = ["p","",line];
		
		}
		
		// Next element
		i = i + 1;
	
	}
	
	// Build index
	quickman_buildIndex();
	
	// Show first page
	quickman_show(0);
	
});

function quickman_buildIndex() {

	var html = '<ul class="sidebar-nav">';

	// Build all sections
	for(var i = 0; i < quickman_data.length; i++) {
		if(quickman_data[i][0] === "t") html += '<li id="quickman-index-'+i+'" class="sidebar-brand"><a href="#" onclick="quickman_show(0);return false;">' + quickman_data[i][2] + '</a></li>';
		else if(quickman_data[i][0] === "s") html += quickman_buildIndex_section(i);
	}
	
	html += '</ul>';
	$("#sidebar-wrapper").html(html);

}

function quickman_buildIndex_section(startIndex) {

	var html = '<li id="quickman-index-'+startIndex+'"><a href="#" onclick="quickman_show('+startIndex+');">' + quickman_data[startIndex][1] + " " + quickman_data[startIndex][2] + '</a><ul>';
	
	for(var i = startIndex+1; i < quickman_data.length; i++) {
		if(quickman_data[i][0] === "ss") html += '<li id="quickman-index-'+i+'"><a href="#"  onclick="quickman_show('+i+');return false;">' + quickman_data[i][1] + ' ' + quickman_data[i][2] + '</a></li>';
		else if(quickman_data[i][0] === "s") break;
	}
	
	html += '</ul></li>';
	return html;

}

function quickman_show(index) {

	$("#sidebar-wrapper li").removeClass("selected");
	$("#sidebar-wrapper li").removeClass("not-selected");
	$("#quickman-index-" + index).addClass("selected");
	$("#sidebar-wrapper li:not(#quickman-index-" + index + ")").addClass("not-selected");

	var html = '<div class="container-fluid"><div class="row"><div class="col-lg-12"><h2>' + quickman_data[index][1] + ' ' + quickman_data[index][2] + '</h2>';

	for(var i = index+1; i < quickman_data.length; i++) {
		if(quickman_data[i][0] === "t" || quickman_data[i][0] === "s" || quickman_data[i][0] === "ss") break;
		else {
		
			if(quickman_data[i][0] === "p") html += '<p>' + quickman_format(quickman_data[i][2]) + '</p>';
			else if(quickman_data[i][0] === "l") html += '<p style="margin-left:40px;">&SmallCircle; &nbsp; ' + quickman_format(quickman_data[i][2]) + '</p>';
			else if(quickman_data[i][0] === "sss") html += '<h3>' + quickman_data[i][2] + '</h3>';
			else if(quickman_data[i][0] === "i") html += '<center style="padding:15px;"><img class="img-responsive" src="' + quickman_data[i][2] + '" /></center>';
		
		}
	}
	
	html += '<hr />';
	html += '<a href="#" style="float:left;margin-right:15px;" onclick="quickman_toggle_index();return false;" class="btn btn-default">Index</a>';
	html += '<a href="#" style="float:right;margin-left:15px;" onclick="quickman_next('+index+');return false;" class="btn btn-default">Next</a>';
	html += '<a href="#" style="float:right;margin-left:15px;" onclick="quickman_previous('+index+');return false;" class="btn btn-default">Previous</a>';
	html += '</div></div></div>';
	
	$("#page-content-wrapper").html(html);
	
}

function quickman_format(line) {

	line = line.replace("{","<i>");
	line = line.replace("}","</i>");
	line = line.replace("[","<b>");
	line = line.replace("]","</b>");
	return line;

}

function quickman_toggle_index() {

	$("#wrapper").toggleClass("toggled");

}

function quickman_previous(fromIndex) {

	var prevIndex = 0;
	for(var i = fromIndex - 1; i >= 0; i--) {
		if(quickman_data[i][0] === "s" || quickman_data[i][0] === "ss") {
			quickman_show(i);
			break;
		}
	}
}

function quickman_next(fromIndex) {

	var nextIndex = 0;
	for(var i = fromIndex + 1; i < quickman_data.length; i++) {
		if(quickman_data[i][0] === "s" || quickman_data[i][0] === "ss") {
			quickman_show(i);
			break;
		}
	}
}