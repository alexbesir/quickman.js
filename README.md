# quickman.js

A super-simple single-file user guide (manual, help or documentation) HTML5 / javascript / Bootstrap template.

Write all text in a simple human-friendly plain-text QM format and let quickman.js build the manual's interface.

## How to use quickman.js

Download project to your location and open index.html with a plain-text editor (eg. Notepad++). The plain-text manual content goes between the `<xmp></xmp>` tags. These already include example content that demonstrates the QM format.

Open index.html with a web browser to see quickman.js in action.